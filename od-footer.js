import { PolymerElement, html } from '@polymer/polymer';

export class OdFooter extends PolymerElement {
    static get is() { return 'od-footer' }
    static get template() {
        return html`
            <style>
                #footer-container {
                    width: 100%;
                    padding-bottom: 3vw;
                    background-color: var(--footer-bg-color, #061B1E);
                    color: var(--footer-color, #3E6E75);
                    font-family: 'Ropa Sans', sans-serif;
                    font-size: 2vw;
                }

                #top-container {
                    padding: 2vw;
                    display: flex;
                    justify-content: space-around;
                    color: var(--top-content-color, #3E6E75);
                }

                #content {
                    display: flex;
                }

                #content-left {
                    color: var(--left-content-color, #3E6E75);
                    width: 50%;
                    padding-left: 4vw;
                    display: flex;
                    flex-direction: column;
                    justify-content: flex-end;
                }

                #content-right {
                    color: var(--right-content-color, #4D9158);
                    width: 50%;
                    padding-right: 4vw;
                    display: flex;
                    flex-direction: column;
                    justify-content: flex-end;
                    align-items: flex-end;
                    text-align: right;
                }            
            </style>
            
            <div id="footer-container">
                <div id="top-container">
                    <slot name="top" id="top"></slot>
                </div>
                <div id="content">
                    <div id="content-left">
                        <div id="left-col">
                            <slot name="left" id="left"></slot>
                        </div>
                    </div>
                    <div id="content-right">
                        <div id="right-col">
                            <slot name="right" id="right"></slot>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }
}
customElements.define( OdFooter.is, OdFooter );